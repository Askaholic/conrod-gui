use conrod::{widget, Positionable, Colorable, Widget, Labelable};
use conrod::color;
use conrod::glium::glutin::VirtualKeyCode;

use crate::{IdStruct, Screen};
use crate::game::{Universe, Planet};

use std::sync::{Arc, Mutex};
use std::collections::HashMap;


widget_ids!(struct Ids {
    text,
    key_binding_text,
    object_count_text,
    planets[]
});
impl IdStruct for Ids {
    fn as_any(&mut self) -> &mut dyn std::any::Any { self }
}

#[derive(Eq)]
struct KeyBinding {
    vkey: VirtualKeyCode,
    description: &'static str
}
impl std::hash::Hash for KeyBinding {
    fn hash<H: std::hash::Hasher>(&self, h: &mut H) {
        self.vkey.hash(h)
    }
}
impl PartialEq for KeyBinding {
    fn eq(&self, other: &Self) -> bool {
        self.vkey == other.vkey
    }
}
impl KeyBinding {
    fn new(vkey: VirtualKeyCode) -> KeyBinding {
        KeyBinding { vkey, description: "" }
    }

    fn description(mut self, description: &'static str) -> Self {
        self.description = description;
        self
    }
}
impl std::fmt::Display for KeyBinding {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?} -> {}", &self.vkey, self.description)
    }
}
fn make_key_bindings() -> HashMap<KeyBinding, &'static Fn(&mut MainScreen)> {
    let mut map: HashMap<KeyBinding, &'static Fn(&mut MainScreen)> = HashMap::new();

    map.insert(
        KeyBinding::new(VirtualKeyCode::Tab).description("Cycle through planets"),
        &MainScreen::cycle_tracked_object);
    map.insert(KeyBinding::new(VirtualKeyCode::Equals).description("Zoom In"), &MainScreen::zoom_in);
    map.insert(KeyBinding::new(VirtualKeyCode::Subtract).description("Zoom Out"), &MainScreen::zoom_out);
    map.insert(KeyBinding::new(VirtualKeyCode::PageUp).description("Track largest (radius) object"), &MainScreen::track_largest);

    map
}

pub struct MainScreen {
    next_screen_id: Option<u8>,
    obj: Arc<Mutex<Universe>>,
    track_object: usize,
    scale: f64,
    key_bindings: HashMap<KeyBinding, &'static Fn(&mut MainScreen)>
}
impl MainScreen {
    pub fn new(obj: Arc<Mutex<Universe>>) -> Self {
        Self {
            next_screen_id: None,
            obj,
            track_object: 0,
            scale: 0.2,
            key_bindings: make_key_bindings()
        }
    }

    fn cycle_tracked_object(&mut self) {
        let len = self.obj.lock().unwrap().objects().len();
        self.track_object = if len == 0 { 0 } else { (self.track_object + 1) % len };
    }
    fn track_largest(&mut self) {
        let universe = self.obj.lock().unwrap();
        let objects = universe.objects();

        if objects.len() == 0 { return; }

        let mut largest = &objects[0];
        let mut largest_i = 0;
        for (i, obj) in objects.iter().enumerate() {
            if obj.radius > largest.radius {
                largest = obj;
                largest_i = i;
            }
        }
        self.track_object = largest_i;
    }

    fn zoom_in(&mut self) { self.scale *= 1.5 }
    fn zoom_out(&mut self) { self.scale /= 1.5 }

}
impl Screen for MainScreen {
    fn new_ids(&self, gen: widget::id::Generator) -> Box<dyn IdStruct> {
        Box::new(Ids::new(gen))
    }

    fn set_widgets(&mut self, ui: &mut conrod::UiCell, ids: &mut Box<dyn IdStruct>) {
        let ids: &mut Ids = ids.as_any().downcast_mut::<Ids>()
            .expect("Wrong type of IdStruct passed to set_widgets");

        let world = self.obj.lock().expect("Mutex got poisoned whoops");
        let planets = world.objects();

        ids.planets.resize(
            world.object_count(), &mut ui.widget_id_generator()
        );

        let zero_planet = Planet::new(0.0, 0.0);
        let tracked = &planets.get(self.track_object).unwrap_or(&zero_planet);
        for (id, planet) in ids.planets.iter().zip(planets) {
            widget::Circle::fill(planet.radius * self.scale)
                .color(get_planet_color(planet))
                .x_y((planet.x - tracked.x) * self.scale, (planet.y - tracked.y) * self.scale)
                .set(*id, ui);
        }

        widget::Text::new(
            &format!(
                "Keybindings:\n{}",
                self.key_bindings.keys().map(|k| k.to_string())
                    .collect::<Vec<String>>().join("\n")
            )
        )
            .align_top_of(ui.window)
            .align_left_of(ui.window)
            .color(conrod::color::DARK_GRAY)
            .font_size(14)
            .set(ids.key_binding_text, ui);

        widget::Text::new(&format!("Object Count: {}", world.object_count()))
            .down_from(ids.key_binding_text, 20.0)
            .align_left_of(ui.window)
            .color(conrod::color::DARK_GRAY)
            .font_size(14)
            .set(ids.object_count_text, ui);

        widget::Text::new("Hello Universe!")
            .align_middle_x_of(ui.window)
            .align_top_of(ui.window)
            .color(conrod::color::BLACK)
            .font_size(32)
            .set(ids.text, ui);
    }

    fn key_pressed(&mut self, event: conrod::glium::glutin::WindowEvent) {
        use conrod::glium::glutin::ElementState;
        use conrod::glium::glutin::WindowEvent::KeyboardInput;

        match event {
            KeyboardInput {
            input: conrod::glium::glutin::KeyboardInput {
                    virtual_keycode: Some(vkey),
                    state: ElementState::Pressed,
                    ..
                },
                ..
            } => {
                if let Some(&func) = self.key_bindings.get(&KeyBinding::new(vkey)) {
                    func(self);
                }
            }
            _ => ()
        }
    }

    fn has_next_screen(&self) -> bool {
        self.next_screen_id.is_some();
        false
    }

    fn get_next_screen(&self) -> Option<Box<dyn Screen>> {
        match self.next_screen_id {
            _ => None
        }
    }
}

pub fn get_planet_color(planet: &Planet) -> color::Color {
    color::Color::Rgba(
        sigmoid(planet.mass, 10000.0) as f32,
        sigmoid(planet.radius, 25.0) as f32,
        sigmoid(std::f64::consts::PI * planet.radius * planet.radius * 5.0/ planet.mass, 1.0) as f32,
        1.0
    )
}

fn sigmoid(x: f64, decay: f64) -> f64 {
    1.0 / (1.0 + std::f64::consts::E.powf(x / decay))
}
