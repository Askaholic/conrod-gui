extern crate rand;

use rand::Rng;

use conrod_gui::{Gui};
use conrod_gui::menus::MainScreen;
use conrod_gui::game::{Planet, Velocity, Universe, Game};
use std::sync::{Arc, Mutex};
use std::sync::mpsc;

use conrod_gui::menus::main_menu::get_planet_color;



fn main() {
    const WIDTH: f64 = 800.0;
    const HEIGHT: f64 = 400.0;

    let mut gen = rand::thread_rng();

    let mut world = Universe::new();
    let coord_range = 50000.0;
    let vel_range = 100.0;
    for _ in 0..2000 {
        world.add_object(
            Planet::new(gen.gen_range(-coord_range, coord_range), gen.gen_range(-coord_range, coord_range))
                .radius(gen.gen_range(0.0, 50.0))
                .density(gen.gen_range(1.0, 10.0))
                .vel(Velocity::new(gen.gen_range(-vel_range, vel_range), gen.gen_range(-vel_range, vel_range)))
        );
    }
    // for obj in world.objects() {
    //     println!("{:?}\n{:?}", obj, get_planet_color(obj));
    // }
    // world.add_object(
    //     Planet::new(100.0, 0.0)
    //         .radius(10.0)
    //         .density(5.0)
    //         .vel(Velocity::new(0.0, 0.0))
    // );
    // world.add_object(
    //     Planet::new(0.0, 100.0)
    //         .radius(50.0)
    //         .density(5.0)
    //         .vel(Velocity::new(0.0, 0.0))
    // );
    let world_mutex = Arc::new(Mutex::new(world));


    let (tx, rx) = mpsc::channel();
    let game_thread = Game::spawn_loop_thread(world_mutex.clone(), rx);

    let mut gui = Gui::new(WIDTH, HEIGHT, "Hello Conrod!");

    gui.set_screen(Box::new(MainScreen::new(world_mutex.clone())));
    gui.run();

    // Exit the game loop
    drop(tx);
    game_thread.join().expect("Failed to join thread");
}
