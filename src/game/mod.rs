use std::time;
use std::sync::{Arc, Mutex};
use std::sync::mpsc;
use std::thread;

pub mod objects;

pub use self::objects::*;

// Universal gravitational constant
const G: f64 = 5.0;
pub type Packet = ();

pub struct Game { }

impl Game {
    pub fn new() -> Game {
        Game {}
    }

    pub fn run_loop(world: Arc<Mutex<Universe>>, rx: mpsc::Receiver<Packet>) {
        let ups = 64;
        let sleep_interval = time::Duration::from_millis(1000 / ups);
        let mut last_update = time::Instant::now();

        loop {
            let duration_since_last_update = time::Instant::now().duration_since(last_update);
            if duration_since_last_update < sleep_interval {
                std::thread::sleep(sleep_interval - duration_since_last_update);
            }

            match rx.try_recv() {
                Ok(_) | Err(mpsc::TryRecvError::Disconnected) => return,
                Err(mpsc::TryRecvError::Empty) => {}
            }

            world.lock().expect("Mutex was poisoned... RIP!").update(ups);

            last_update = time::Instant::now();
        }
    }

    pub fn spawn_loop_thread(world: Arc<Mutex<Universe>>, rx: mpsc::Receiver<Packet>) -> thread::JoinHandle<()> {
        thread::spawn(move || {
            Game::run_loop(world, rx);
        })
    }
}
