use euclid::TypedVector2D;
use super::G;


pub struct WorldSpacePerSecond;

pub type Velocity = TypedVector2D<f64, WorldSpacePerSecond>;

pub struct Universe {
    objects: Vec<Planet>
}

impl Universe {
    pub fn new() -> Universe {
        Universe { objects: vec![] }
    }

    pub fn add_object(&mut self, obj: Planet) {
        self.objects.push(obj);
    }

    /// ups: updates per second
    pub fn update(&mut self, ups: u64) {
        // Merge intersecting planets
        let mut to_delete = vec![false; self.objects.len()];
        let mut new_objects = Vec::new();
        for i in 0..self.objects.len() {
            for j in i+1..self.objects.len() {
                if to_delete[i] || to_delete[j] { continue; }
                let (p1, p2) = (&self.objects[i], &self.objects[j]);
                if !p1.intersects(p2) { continue }

                let bigger = if p1.radius > p2.radius { p1 } else { p2 };
                let mut combined = (*bigger).clone();
                combined.merge(p2);
                new_objects.push(combined);
                to_delete[i] = true;
                to_delete[j] = true;
            }
        }

        self.objects = self.objects.iter()
                        .zip(to_delete.iter())
                        .filter(|(_, &is_deleted)| !is_deleted)
                        .map(|(obj, _)| obj.clone())
                        .collect();
        self.objects.append(&mut new_objects);
        self.objects.sort_unstable_by(|a, b| (b.radius as i64).cmp(&(a.radius as i64)));

        // Calculate new positions
        let velocities = self.objects.iter()
            .map(|obj| self.get_velocity_from_field(obj.x, obj.y))
            .collect::<Vec<Velocity>>();
        self.objects.iter_mut().zip(velocities.iter())
            .map(|(obj, vel)| {
                obj.vel += *vel;
                obj.update(ups);
            })
            .count();
    }

    fn get_velocity_from_field(&self, x: f64, y: f64) -> Velocity {
        let mut vel = Velocity::new(0.0, 0.0);
        for obj in self.objects.iter() {
            vel += obj.get_gravitational_velocity(x, y);
        }
        vel
    }

    pub fn objects(&self) -> &Vec<Planet> {
        &self.objects
    }

    pub fn object_count(&self) -> usize {
        self.objects.len()
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Planet {
    pub x: f64,
    pub y: f64,
    pub mass: f64,
    pub radius: f64,
    pub vel: Velocity
}

impl Planet {
    pub fn new(x: f64, y: f64) -> Planet {
        Planet {
            x, y, mass: 0.0, radius: 0.0,
            vel: Velocity::new(0.0, 0.0)
        }
    }

    pub fn radius(mut self, radius: f64) -> Planet {
        self.radius = radius;
        self
    }

    pub fn density(mut self, density: f64) -> Planet {
        let mass = std::f64::consts::PI * self.radius * self.radius * density;
        self.mass = mass;
        self
    }

    pub fn vel(mut self, vel: Velocity) -> Planet {
        self.vel = vel;
        self
    }

    pub fn update(&mut self, ups: u64) {
        self.x += self.vel.x / ups as f64;
        self.y += self.vel.y / ups as f64;
    }

    pub fn get_gravitational_velocity(&self, x: f64, y: f64) -> Velocity {
        let dx = self.x - x;
        let dy = self.y - y;
        let r2 = dx * dx + dy * dy;
        if r2 == 0.0 { return Velocity::new(0.0, 0.0); }
        let magnitude = self.mass * G / r2;
        Velocity::new(dx, dy).normalize() * magnitude
    }

    pub fn intersects(&self, other: &Self) -> bool {
        (self.x - other.x).powf(2.0) + (self.y - other.y).powf(2.0) < (self.radius + other.radius).powf(2.0)
    }

    pub fn merge(&mut self, other: &Self) {
        let combined_mass = self.mass + other.mass;
        self.radius += other.radius.sqrt().sqrt();
        // (m1v1 + m2v2) / (m1 + m2)
        self.vel = ((self.vel * self.mass) + (other.vel * other.mass)) / combined_mass;
        self.mass = combined_mass;
    }
}
