#[macro_use] extern crate conrod;
extern crate find_folder;
extern crate euclid;

use conrod::backend::glium::glium::{self, Surface};
use conrod::widget;
use std::any::Any;
use conrod::color;

pub mod menus;
pub mod game;


pub trait IdStruct {
    fn as_any(&mut self) -> &mut dyn Any;
}

pub trait Screen {
    fn new_ids(&self, gen: widget::id::Generator) -> Box<dyn IdStruct>;
    fn set_widgets(&mut self, ui: &mut conrod::UiCell, ids: &mut Box<dyn IdStruct>);
    fn has_next_screen(&self) -> bool { false }
    fn get_next_screen(&self) -> Option<Box<dyn Screen>> { None }
    fn key_pressed(&mut self, _event: glium::glutin::WindowEvent) {}
}

widget_ids!(struct EmptyIds {});
impl IdStruct for EmptyIds {
    fn as_any(&mut self) -> &mut dyn Any { self }
}
struct EmptyScreen {}
impl Screen for EmptyScreen {
    fn new_ids(&self, gen: widget::id::Generator) -> Box<dyn IdStruct> {
        Box::new(EmptyIds::new(gen))
    }

    fn set_widgets(&mut self, _ui: &mut conrod::UiCell, _ids: &mut Box<dyn IdStruct>) {}
}

fn get_default_theme() -> conrod::theme::Theme {
    conrod::theme::Theme {
        name: String::from("Conrod D1"),
        background_color: color::BLUE,
        shape_color: color::Color::Rgba(25.0 / 255.0, 54.0 / 255.0, 107.0 / 255.0, 1.0),
        ..conrod::theme::Theme::default()
    }
}

struct KeyPresses {
    fullscreen_pressed: bool
}
impl KeyPresses {
    fn new() -> KeyPresses {
        KeyPresses {
            fullscreen_pressed: false
        }
    }
}
pub struct Gui {
    renderer: conrod::backend::glium::Renderer,
    ui: conrod::Ui,
    display: conrod::glium::Display,
    image_map: conrod::image::Map<glium::texture::Texture2d>,
    event_loop: EventLoop,
    current_screen: Box<dyn Screen>,
    is_fullscreen: bool,
    key_presses: KeyPresses
}

impl Gui {
    pub fn new(width: f64, height: f64, title: &str) -> Self {
        let events_loop = glium::glutin::EventsLoop::new();
        let window = glium::glutin::WindowBuilder::new()
                        .with_title(title)
                        .with_dimensions(glium::glutin::dpi::LogicalSize::new(width, height));
        let context = glium::glutin::ContextBuilder::new()
                        .with_vsync(true)
                        .with_multisampling(4);
        let display = glium::Display::new(window, context, &events_loop).unwrap();

        let mut ui = conrod::UiBuilder::new([width, height])
            .theme(get_default_theme())
            .build();

            let assets = find_folder::Search::KidsThenParents(3, 5)
                .for_folder("assets")
                .unwrap();
            let font_path = assets.join("fonts/NotoSans/NotoSans-Regular.ttf");
            ui.fonts.insert_from_file(font_path).unwrap();

        Gui {
            renderer: conrod::backend::glium::Renderer::new(&display).unwrap(),
            image_map: conrod::image::Map::<glium::texture::Texture2d>::new(),
            ui,
            display,
            event_loop: EventLoop::new(events_loop, 60),
            current_screen: Box::new(EmptyScreen {}),
            is_fullscreen: false,
            key_presses: KeyPresses::new(),
        }
    }

    pub fn set_screen(&mut self, screen: Box<dyn Screen>) {
        self.current_screen = screen;
    }

    pub fn run(&mut self) {
        let mut ids = self.current_screen.new_ids(self.ui.widget_id_generator());

        loop {
            if self.handle_events() { return; }

            if self.current_screen.has_next_screen() {
                self.current_screen = Box::from(self.current_screen.get_next_screen().unwrap());
                ids = self.current_screen.new_ids(self.ui.widget_id_generator());
            }

            self.current_screen.set_widgets(&mut self.ui.set_widgets(), &mut ids);

            self.draw();
        }
    }

    fn handle_events(&mut self) -> bool {
        for event in self.event_loop.next() {
            if let Some(event) = conrod::backend::winit::convert_event(event.clone(), &self.display) {
                self.ui.handle_event(event);
            }

            if let glium::glutin::Event::WindowEvent { event, ..} = event {
                use conrod::glium::glutin::WindowEvent::{CloseRequested, KeyboardInput};
                use conrod::glium::glutin::VirtualKeyCode;

                if let KeyboardInput {..} = event {
                     self.current_screen.key_pressed(event.clone());
                }

                match event {
                    CloseRequested | KeyboardInput {
                        input: glium::glutin::KeyboardInput {
                            virtual_keycode: Some(VirtualKeyCode::Escape),
                            ..
                        },
                        ..
                    } => return true,
                    KeyboardInput {
                        input: glium::glutin::KeyboardInput {
                            modifiers: glium::glutin::ModifiersState {
                                ctrl: true,
                                ..
                            },
                            virtual_keycode: Some(VirtualKeyCode::F),
                            state: glium::glutin::ElementState::Pressed,
                            ..
                        },
                        ..
                    } => {
                        if self.key_presses.fullscreen_pressed {
                            continue;
                        }
                        self.is_fullscreen = !self.is_fullscreen;
                        self.display.gl_window().set_fullscreen(
                            match self.is_fullscreen {
                                true => Some(self.display.gl_window().get_current_monitor()),
                                false => None
                            }
                        );
                        self.key_presses.fullscreen_pressed = true;
                    },
                    KeyboardInput {
                        input: glium::glutin::KeyboardInput {
                            virtual_keycode: Some(VirtualKeyCode::F),
                            state: glium::glutin::ElementState::Released,
                            ..
                        },
                        ..
                    } => self.key_presses.fullscreen_pressed = false,
                    _ => (),
                }
            }
        }
        false
    }

    fn draw(&mut self) {
        // Render the `Ui` and then display it on the screen.
        if let Some(primitives) = self.ui.draw_if_changed() {
            self.renderer.fill(&self.display, primitives, &self.image_map);
            let mut target = self.display.draw();
            target.clear_color(1.0, 1.0, 1.0, 1.0);
            self.renderer.draw(&self.display, &mut target, &self.image_map).unwrap();
            target.finish().unwrap();
        }
    }
}

struct EventLoop {
    sleep_interval: std::time::Duration,
    last_update: std::time::Instant,
    events_loop: glium::glutin::EventsLoop
}

impl EventLoop {
    fn new(events_loop: glium::glutin::EventsLoop, fps: u64) -> Self {
        EventLoop {
            sleep_interval: std::time::Duration::from_millis(1000 / fps),
            last_update: std::time::Instant::now(),
            events_loop
        }
    }

    fn next(&mut self) -> Vec<glium::glutin::Event> {
        let duration_since_last_update = std::time::Instant::now().duration_since(self.last_update);
        if duration_since_last_update < self.sleep_interval {
            std::thread::sleep(self.sleep_interval - duration_since_last_update);
        }

        // Collect all pending events.
        let mut events = Vec::new();
        self.events_loop.poll_events(|event| events.push(event));

        self.last_update = std::time::Instant::now();

        events
    }
}
